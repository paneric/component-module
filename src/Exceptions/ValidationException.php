<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Exceptions;

use Exception;

class ValidationException extends Exception
{
    public function getReport(): array
    {
        return [];
    }
}
