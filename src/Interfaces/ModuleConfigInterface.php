<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Interfaces;

interface ModuleConfigInterface
{
    public function getOneById(): array;

    public function getOneBy(): array;

    public function getAll(): array;

    public function getAllBy(): array;

    public function getAllByExt(): array;

    public function getAllPaginated(): array;

    public function create(): array;

    public function createMultiple(): array;

    public function update(): array;

    public function updateMultiple(): array;

    public function delete(): array;

    public function deleteMultiple(): array;

    public function repository(): array;

    public function persister(): array;

    public function query(): array;
}
