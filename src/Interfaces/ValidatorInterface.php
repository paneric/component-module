<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Interfaces;

use Paneric\ComponentModule\Exceptions\ValidationException;
use Paneric\ComponentModule\Model\Interfaces\DataObjectInterface;

interface ValidatorInterface
{
    /**
     * @throws ValidationException
     */
    public function invoke(DataObjectInterface|array $dao): void;
}
