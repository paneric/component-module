<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface UpdateApiActionInterface
{
    public function __invoke(Request $request, string $id): ?array;
    public function getStatus(): int;
}
