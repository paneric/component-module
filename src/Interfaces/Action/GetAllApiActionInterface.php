<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetAllApiActionInterface
{
    public function __invoke(Request $request): array;
    public function getStatus(): int;
}
