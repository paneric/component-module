<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Interfaces\Action;

use Psr\Http\Message\ServerRequestInterface as Request;

interface GetOneByApiActionInterface
{
    public function __invoke(Request $request, string $field, string $value): ?array;
    public function getStatus(): int;
}
