<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Interfaces;

use Psr\Http\Message\RequestInterface as Request;

interface ActionListInterface
{
    public function __invoke(Request $request = null): array;
}
