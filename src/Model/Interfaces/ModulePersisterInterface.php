<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Model\Interfaces;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface ModulePersisterInterface
{
    public function createUnique(array $criteria, DataObjectInterface $dataObject): ?string;
    public function createUniqueMultiple(array $criteria, array $dataObjects): array;

    public function updateUnique(array $criteriaSelect, array $criteriaUpdate, DataObjectInterface $dataObject): ?int;
    public function updateUniqueMultiple(array $criteriaSelect, array $criteriaUpdate, array $dataObjects): array;

    public function delete(array $criteria): int;
    public function deleteMultiple(array $criteriaDelete): array;
}
