<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Model\Interfaces;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface ModuleQueryInterface
{
    public function queryBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function queryOneBy(array $criteria): ?DataObjectInterface;
}
