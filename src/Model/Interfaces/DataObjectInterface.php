<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Model\Interfaces;

interface DataObjectInterface
{
    public function hydrate(array $attributes, bool $isSc = true): array;

    public function convert(bool $isSc = true): array;

    public function prepare(): array;
}
