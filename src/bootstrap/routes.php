<?php

declare(strict_types=1);

use Paneric\ComponentModule\Action\CreateApiAction;
use Paneric\ComponentModule\Action\CreateMultipleApiAction;
use Paneric\ComponentModule\Action\DeleteApiAction;
use Paneric\ComponentModule\Action\DeleteMultipleApiAction;
use Paneric\ComponentModule\Action\GetAllApiAction;
use Paneric\ComponentModule\Action\GetAllByApiAction;
use Paneric\ComponentModule\Action\GetAllPaginatedApiAction;
use Paneric\ComponentModule\Action\GetAllByExtendedApiAction;
use Paneric\ComponentModule\Action\GetOneByIdApiAction;
use Paneric\ComponentModule\Action\GetOneByApiAction;
use Paneric\ComponentModule\Action\UpdateApiAction;
use Paneric\ComponentModule\Action\UpdateMultipleApiAction;
use Paneric\ComponentModule\Controller\ApiModuleController;
use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($slim, $container, $module, $proxyPrefix)) {

    $routePrefix = $proxyPrefix  . '/' . $module;

    try {
        $slim->post($routePrefix . '/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->create(
                $request,
                $response,
                $this->get(CreateApiAction::class)
            );
        })->setName($module . '.add');

        $slim->post($routePrefix . 's/add', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->createMultiple(
                $request,
                $response,
                $this->get(CreateMultipleApiAction::class)
            );
        })->setName($module . 's.add');


        $slim->delete($routePrefix . '/remove/{id}', function (Request $request, Response $response, array $args) {
            return $this->get(ApiModuleController::class)->delete(
                $request,
                $response,
                $this->get(DeleteApiAction::class),
                $args['id']
            );
        })->setName($module . '.remove');

        $slim->post($routePrefix . 's/remove', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->deleteMultiple(
                $request,
                $response,
                $this->get(DeleteMultipleApiAction::class)
            );
        })->setName($module . 's.remove');


        $slim->get($routePrefix . 's/get', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAll(
                $request,
                $response,
                $this->get(GetAllApiAction::class)
            );
        })->setName($module . 's.get');

        $slim->post($routePrefix . 's/get-ext', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->getAllByExtended(
                $request,
                $response,
                $this->get(GetAllByExtendedApiAction::class)
            );
        })->setName($module . 's.get-ext');

        $slim->get($routePrefix . 's/get/{field}/{value}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->getAllBy(
                $request,
                $response,
                $this->get(GetAllByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName($module . 's.get-by');

        $slim->get($routePrefix . 's/get-paginated[/{page}]', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->getAllPaginated(
                $request,
                $response,
                $this->get(GetAllPaginatedApiAction::class),
                $args['page'] ?? 1
            );
        })->setName($module . 's.get-paginated')
            ->addMiddleware($container->get(PaginationMiddleware::class));

        $slim->get($routePrefix . '/get/{id}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->getOneById(
                $request,
                $response,
                $this->get(GetOneByIdApiAction::class),
                $args['id']
            );
        })->setName($module . '.get-by-id');

        $slim->get($routePrefix . '/get/{field}/{value}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->getOneBy(
                $request,
                $response,
                $this->get(GetOneByApiAction::class),
                $args['field'],
                $args['value']
            );
        })->setName($module . '.get-by');


        $slim->put($routePrefix . '/edit/{id}', function (
            Request $request,
            Response $response,
            array $args
        ) {
            return $this->get(ApiModuleController::class)->update(
                $request,
                $response,
                $this->get(UpdateApiAction::class),
                $args['id']
            );
        })->setName($module . '.edit');

        $slim->put($routePrefix . 's/edit', function (Request $request, Response $response) {
            return $this->get(ApiModuleController::class)->updateMultiple(
                $request,
                $response,
                $this->get(UpdateMultipleApiAction::class)
            );
        })->setName($module . 's.edit');
    } catch (Exception $e) {
    }
}
