<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Interfaces\Action\GetAllByApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllByApiAction extends Action implements GetAllByApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    protected int $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAllBy();
    }

    public function __invoke(Request $request, string $field, string $value): array
    {
        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $this->getAllBy($request, $field, urldecode($value)),
        ];
    }

    public function getAllBy(Request $request, string $field, string $value): array
    {
        $queryParams = $request->getQueryParams();

        $orderBy = $this->config['order_by'];

        $collection = $this->adapter->findBy(
            [$this->config['prefix'] . $field => $value],
            $orderBy($queryParams['field'] ?? 'id')
        );

        return $this->arrangeObjectsCollectionById($collection, true);
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
