<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Exceptions\ValidationException;
use Paneric\ComponentModule\Infrastructure\Event\CreateFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeCreateEvent;
use Paneric\ComponentModule\Infrastructure\Event\CreateSuccessEvent;
use Paneric\ComponentModule\Interfaces\Action\CreateMultipleApiActionInterface;
use Paneric\ComponentModule\Interfaces\GuardInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class CreateMultipleApiAction extends Action implements CreateMultipleApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected GuardInterface $guard;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        GuardInterface $guard,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->createMultiple();
        $this->guard = $guard;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(Request $request): ?array
    {
        $collection = $this->createCollectionFromAttributes(
            $request->getParsedBody(),
            $this->config['dto_class'],
            true
        );

        $createUniqueCriteria = $this->config['create_unique_criteria'];

        try {
            $this->status = 400;

            new $this->config['vld_class']($collection);

            if ($this->config['string_id']) {
                $guard = $this->guard;
                array_walk($collection, static function ($item) use ($guard) {
                    $item->setId($guard->setUniqueId());
                });
            }

            $criteria = $createUniqueCriteria($collection);

            $this->eventDispatcher->dispatch(new BeforeCreateEvent($collection));
            $createSuccessEvent = new CreateSuccessEvent($collection);
            $collection = $this->adapter->createUniqueMultiple($criteria, $collection);

            if (empty($collection)) {
                $this->eventDispatcher->dispatch($createSuccessEvent);

                $this->status = 201;

                return [
                    'status' => $this->status,
                ];
            }

            $this->eventDispatcher->dispatch(new CreateFailureEvent($collection));

            return [
                'status' => $this->status,
                'error' => 'db_create_multiple_error',
                'body' => $this->convertCollection($collection),
            ];
        } catch (ValidationException $e) {
            return [
                'status' => $this->status,
                'error' => 'validation_create_multiple_error',
                'body' => $e->getReport(),
            ];
        }
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
