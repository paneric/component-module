<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleQueryInterface;
use Paneric\CSRTriad\Action;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllJoinedApiAction extends Action
{
    protected ModuleQueryInterface $adapter;
    protected array $config;

    protected int $status;

    public function __construct(
        ModuleQueryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAll();
    }

    public function __invoke(Request $request): array
    {
        $queryParams = $request->getQueryParams();

        $orderBy = $this->config['order_by'];

        $collection = $this->adapter->queryBy(
            [],
            $orderBy($queryParams['field'] ?? 'id')
        );

        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $this->arrangeObjectsCollectionById($collection, true),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
