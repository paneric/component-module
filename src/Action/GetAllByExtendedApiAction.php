<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Interfaces\Action\GetAllByExtendedApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllByExtendedApiAction extends Action implements GetAllByExtendedApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    protected int $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAllByExt();
    }

    public function __invoke(Request $request): array
    {
        $this->status = 200;

        $attributes = $request->getParsedBody();

        $mode = $attributes['mode'];
        unset($attributes['query_mode']);

        $findByCriteria = $this->config['find_by_criteria'];
        $criteria = $findByCriteria($attributes);

        $orderByCriteria = $this->config['order_by_criteria'];
        $orderBy = $orderByCriteria($attributes);

        return [
            'status' => $this->status,
            'body' => $this->getAllByExtended($request, $mode, $criteria, $orderBy),
        ];
    }

    public function getAllByExtended(Request $request, string $mode, array $criteria, array $orderBy): array
    {
        $queryParams = $request->getQueryParams();

        $collection = $this->adapter->{'findBy' . lcfirst($mode)}(
            $criteria,
            $orderBy
        );

        return $this->arrangeObjectsCollectionById($collection, true);
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
