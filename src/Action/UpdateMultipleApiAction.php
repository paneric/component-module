<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Exceptions\ValidationException;
use Paneric\ComponentModule\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\ComponentModule\Infrastructure\Event\UpdateSuccessEvent;
use Paneric\ComponentModule\Interfaces\Action\UpdateMultipleApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class UpdateMultipleApiAction extends Action implements UpdateMultipleApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->updateMultiple();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(Request $request): ?array
    {
        $this->status = 200;

        $collection = $this->createCollectionFromAttributes(
            $request->getParsedBody(),
            $this->config['dto_class'],
            true
        );

        $findByCriteria = $this->config['find_by_criteria'];
        $updateUniqueCriteria = $this->config['update_unique_criteria'];

        $criteriaSelect = $findByCriteria($collection);
        $criteriaUpdate = $updateUniqueCriteria($collection);

        try {
            $this->status = 400;

            new $this->config['vld_class']($collection);

            $this->eventDispatcher->dispatch(new BeforeUpdateEvent($collection));
            $updateSuccessEvent = new UpdateSuccessEvent($collection);
            $collection = $this->adapter->updateUniqueMultiple(
                $criteriaSelect,
                $criteriaUpdate,
                $collection
            );

            if (empty($collection)) {
                $this->eventDispatcher->dispatch($updateSuccessEvent);

                $this->status = 200;

                return [
                    'status' => $this->status,
                ];
            }

            $this->eventDispatcher->dispatch(new UpdateFailureEvent($collection));

            return [
                'status' => $this->status,
                'error' => 'db_update_multiple_unique_error',
                'body' => $this->convertCollection($collection),
            ];
        } catch (ValidationException $e) {
            return [
                'status' => $this->status,
                'error' => 'validation_update_multiple_error',
                'body' => $e->getReport(),
            ];
        }
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
