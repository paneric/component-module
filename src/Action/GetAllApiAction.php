<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Interfaces\Action\GetAllApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllApiAction extends Action implements GetAllApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    protected int $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAll();
    }

    public function __invoke(Request $request): array
    {
        $this->status = 200;

        return [
            'status' => $this->status,
            'body' => $this->getAll($request),
        ];
    }

    public function getAll(Request $request): array
    {
        $queryParams = $request->getQueryParams();

        $orderBy = $this->config['order_by'];

        $collection = $this->adapter->findBy(
            [],
            $orderBy($queryParams['field'] ?? 'id')
        );

        return $this->arrangeObjectsCollectionById($collection, true);
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
