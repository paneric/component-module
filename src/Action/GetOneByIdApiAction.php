<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Interfaces\Action\GetOneByIdApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetOneByIdApiAction extends Action implements GetOneByIdApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    protected int $status;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getOneById();
    }

    public function __invoke(Request $request, string $id): ?array
    {
        $findOneByCriteria = $this->config['find_one_by_criteria'];

        $dto = $this->adapter->findOneBy($findOneByCriteria($id));

        if ($dto ===  null) {
            $this->status = 400;

            return [
                'status' => $this->status,
                'error' => 'resource_not_found'
            ];
        }

        $this->status = 200;

        $dtos = $this->arrangeObjectsCollectionById([$dto], true);

        return [
            'status' => $this->status,
            'body' => array_shift(
                $dtos
            ),
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
