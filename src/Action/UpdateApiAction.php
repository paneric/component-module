<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Exception;
use Paneric\ComponentModule\Exceptions\ValidationException;
use Paneric\ComponentModule\Infrastructure\Event\UpdateFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeUpdateEvent;
use Paneric\ComponentModule\Interfaces\Action\UpdateApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;

class UpdateApiAction extends Action implements UpdateApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->update();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(Request $request, string $id): ?array
    {
        $dto = new $this->config['dto_class']();
        $dto->hydrate(
            $request->getParsedBody()
        );

        $findOneByCriteria = $this->config['find_one_by_criteria'];
        $updateUniqueCriteria = $this->config['update_unique_criteria'];

        try {
            $this->status = 200;

            new $this->config['vld_class']($dto);

            try {
                $this->eventDispatcher->dispatch(new BeforeUpdateEvent($dto));
                $updateResult = $this->adapter->updateUnique(
                    $findOneByCriteria($dto, $id),
                    $updateUniqueCriteria($id),
                    $dto
                );

                if ($updateResult === null) {
                    throw new Exception('db_update_unique_error');
                }

                $this->eventDispatcher->dispatch(new UpdateFailureEvent($dto));

                return [
                    'status' => $this->status,
                ];
            } catch (Throwable $e) {
                $this->eventDispatcher->dispatch(new UpdateFailureEvent($dto));

                $this->status = 400;

                return [
                    'status' => $this->status,
                    'error' => $e->getMessage(),
                ];
            }
        } catch (ValidationException $e) {
            $this->status = 200;

            return [
                'status' => $this->status,
                'error' => $e->getReport(),
            ];
        }
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
