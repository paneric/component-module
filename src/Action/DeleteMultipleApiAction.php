<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Infrastructure\Event\DeleteFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeDeleteEvent;
use Paneric\ComponentModule\Infrastructure\Event\DeleteSuccessEvent;
use Paneric\ComponentModule\Interfaces\Action\DeleteMultipleApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class DeleteMultipleApiAction extends Action implements DeleteMultipleApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->deleteMultiple();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(Request $request): ?array
    {
        $this->status = 400;

        $collection = $this->createCollectionFromAttributes(
            $request->getParsedBody(),
            $this->config['dto_class'],
            true
        );

        $deleteByCriteria = $this->config['delete_by_criteria'];

        $this->eventDispatcher->dispatch(new BeforeDeleteEvent($collection));
        $deleteSuccessEvent = new DeleteSuccessEvent($collection);
        $collection = $this->adapter->deleteMultiple(
            $deleteByCriteria($collection)
        );

        if (empty($collection)) {
            $this->eventDispatcher->dispatch($deleteSuccessEvent);

            $this->status = 200;

            return [
                'status' => $this->status,
            ];
        }

        $this->eventDispatcher->dispatch(new DeleteFailureEvent($collection));

        return [
            'status' => $this->status,
            'error' => 'db_delete_multiple_error',
            'body' => $collection,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
