<?php

namespace Paneric\ComponentModule\Action\Config;

use Closure;
use JetBrains\PhpStorm\ArrayShape;

trait ModuleConfigTrait
{
    #[ArrayShape([
        'find_one_by_criteria' => Closure::class
    ])]
    public function getOneById(): array
    {
        return [
            'find_one_by_criteria' => function (int|string $id): array {
                return [$this->prefix . 'id' => $id];
            },
        ];
    }

    #[ArrayShape([
        'find_one_by_criteria' => Closure::class
    ])]
    public function getOneBy(): array
    {
        return [
            'find_one_by_criteria' => function (string $field, int|string $value): array {
                return [$this->prefix . $field => $value];
            },
        ];
    }

    #[ArrayShape([
        'order_by' => Closure::class
    ])]
    public function getAll(): array
    {
        return [
            'order_by' => function (string $orderByField): array {
                return [$this->prefix . $orderByField => 'ASC'];
            },
        ];
    }

    #[ArrayShape([
        'order_by' => Closure::class,
        'prefix' => "string"
    ])]
    public function getAllBy(): array
    {
        return [
            'order_by' => function (string $orderByField): array {
                return [$this->prefix . $orderByField => 'ASC'];
            },
            'prefix' => $this->prefix,
        ];
    }

    #[ArrayShape([
        'find_by_criteria' => Closure::class,
        'order_by_criteria' => Closure::class
    ])]
    public function getAllByExt(): array
    {
        return [
            'find_by_criteria' => function (array $attributes, string $local = null): array {
                $attributes = [
                    'field' => $attributes['criteria_field'] ?? [],
                    'value' => $attributes['criteria_value'] ?? [],
                ];
                $criteria = [];
                $fields = $attributes['field'];
                $values = $attributes['value'];
                if (is_array($fields)) {
                    foreach ($fields as $key => $field) {
                        $criteria[$this->prefix . $field] = $values[$key];
                    }
                    return $criteria;
                }
                foreach ($values as $ref => $value) {
                    $criteria[$this->prefix . $fields][$ref] = $value;
                }
                return $criteria;
            },
            'order_by_criteria' => function (array $attributes, string $orderByField = null): array {
                $attributes = [
                    'field' => $attributes['order_by_field'] ?? [],
                    'value' => $attributes['order_by_value'] ?? [],
                ];
                return [];
            },
        ];
    }

    #[ArrayShape([
        'find_by_criteria' => Closure::class,
        'order_by' => Closure::class
    ])]
    public function getAllPaginated(): array
    {
        return [
            'find_by_criteria' => function (string $local = null): array {
                return [];
            },
            'order_by' => function (string $orderByField): array {
                return [$this->prefix . $orderByField => 'ASC'];
            },
        ];
    }

    #[ArrayShape([
        'dto_class' => "string",
        'vld_class' => "string",
        'create_unique_criteria' => Closure::class,
        'string_id' => "bool"
    ])]
    public function create(): array
    {
        return [
            'dto_class' => $this->daoClass,
            'vld_class' => $this->vldClass,
            'create_unique_criteria' => function (array $attributes): array {
                return [$this->prefix . $this->uniqueKey => $attributes[$this->uniqueKey]];
            },
            'string_id' => $this->stringId,
        ];
    }

    #[ArrayShape([
        'delete_by_criteria' => Closure::class
    ])]
    public function delete(): array
    {
        return [
            'delete_by_criteria' => function (array $attributes): array {
                $deleteByCriteria = [];

                foreach ($attributes as $key => $value) {
                    $deleteByCriteria[$this->prefix  . $key] = $value;
                }

                return $deleteByCriteria;
            },
        ];
    }

    #[ArrayShape([
        'dto_class' => "string",
        'vld_class' => "string",
        'delete_by_criteria' => Closure::class
    ])]
    public function deleteMultiple(): array
    {
        return [
            'dto_class' => $this->daoClass,
            'vld_class' => $this->vldClass,
            'delete_by_criteria' => function (array $collection): array {
                $deleteByCriteria = [];

                foreach ($collection as $index => $dao) {
                    $deleteByCriteria[$index][$this->prefix . 'id'] = $dao->getId();
                }

                return $deleteByCriteria;
            },
        ];
    }

    protected function hydrateField(): string
    {
        if (str_contains($this->uniqueKey, '_')) {
            return str_replace(' ', '', ucwords(str_replace('_', ' ', $this->uniqueKey)));
        }
        return $this->uniqueKey;
    }

    private function prepareCreateWhereCondition(): string
    {
        return $this->prepareWhereCondition(false);
    }

    private function prepareUpdateWhereCondition(): string
    {
        return $this->prepareWhereCondition(true);
    }

    private function prepareWhereCondition(bool $withNotIn): string
    {
        $whereCondition = '';
        if (!empty($this->uniqueKeys)) {
            foreach ($this->uniqueKeys as $ii => $uniqueKey) {
                if ($ii === 0) {
                    $condition = sprintf(
                        ' WHERE %s=:%s',
                        $this->prefix . $uniqueKey,
                        $this->prefix . $uniqueKey
                    );
                    $whereCondition .= $condition;
                    continue;
                }
                $condition = sprintf(
                    ' AND %s=:%s',
                    $this->prefix . $uniqueKey,
                    $this->prefix . $uniqueKey
                );
                $whereCondition .= $condition;
            }
        }

        if (!$withNotIn) {
            return $whereCondition;
        }

        $condition = sprintf(' AND %sid NOT IN (:%sid)', $this->prefix, $this->prefix);
        $whereCondition .= $condition;

        return $whereCondition;
    }
}
