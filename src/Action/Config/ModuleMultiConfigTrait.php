<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action\Config;

use Closure;
use JetBrains\PhpStorm\ArrayShape;
use PDO;

trait ModuleMultiConfigTrait
{
    use ModuleConfigTrait;

    #[ArrayShape([
        'dto_class' => "string",
        'vld_class' => "string",
        'create_unique_criteria' => Closure::class,
        'string_id' => "bool"
    ])]
    public function createMultiple(): array
    {
        return [
            'dto_class' => $this->daoClass,
            'vld_class' => $this->vldClass,
            'create_unique_criteria' => function (array $collection): array {
                $createUniqueCriteria = [];

                foreach ($collection as $index => $dao) {
                    foreach ($this->uniqueKeys as $uniqueKey) {
                        $createUniqueCriteria[$index] = [
                            $this->prefix . $uniqueKey => $dao->{'get' . ucfirst($this->hydrateField())}()
                        ];
                    }
                }

                return $createUniqueCriteria;
            },
            'string_id' => $this->stringId,
        ];
    }

    #[ArrayShape([
        'dto_class' => "string",
        'vld_class' => "string",
        'find_one_by_criteria' => Closure::class,
        'update_unique_criteria' => Closure::class
    ])]
    public function update(): array
    {
        return [
            'dto_class' => $this->daoClass,
            'vld_class' => $this->vldClass,
            'find_one_by_criteria' => function ($dao, int|string $id): array {
                $findOneByCriteria [$this->prefix . 'id'] = $id;

                foreach ($this->uniqueKeys as $uniqueKey) {
                    $findOneByCriteria[$this->prefix . $uniqueKey] = $dao->{'get' . ucfirst($this->hydrateField())}();
                }

                return $findOneByCriteria;
            },
            'update_unique_criteria' => function (int|string $id): array {
                return [$this->prefix . 'id' => $id];
            },
        ];
    }

    #[ArrayShape([
        'dto_class' => "string",
        'vld_class' => "string",
        'find_by_criteria' => Closure::class,
        'update_unique_criteria' => Closure::class
    ])]
    public function updateMultiple(): array
    {
        return [
            'dto_class' => $this->daoClass,
            'vld_class' => $this->vldClass,
            'find_by_criteria' => function (array $collection): array {
                $findByCriteria = [];

                foreach ($collection as $index => $dao) {
                    $findByCriteria[$index][$this->prefix . 'id'] = $dao->getId();
                    foreach ($this->uniqueKeys as $uniqueKey) {
                        $findByCriteria[$index] = [
                            $this->prefix . $uniqueKey => $dao->{'get' . ucfirst($this->hydrateField())}(),
                        ];
                    }
                }

                return $findByCriteria;
            },
            'update_unique_criteria' => function (array $collection): array {
                $updateUniqueCriteria = [];

                foreach ($collection as $index => $dao) {
                    $updateUniqueCriteria[$index] = [
                        $this->prefix . 'id' => $dao->getId(),
                    ];
                }

                return $updateUniqueCriteria;
            },
        ];
    }

    #[ArrayShape([
        'table' => "string",
        'dto_class' => "string",
        'select_query' => "string",
        'fetch_mode' => "int"
    ])]
    public function repository(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->daoClass,
            'select_query' => $this->query,
            'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
        ];
    }

    #[ArrayShape([
        'table' => "string",
        'dto_class' => "string",
        'fetch_mode' => "int",
        'create_unique_where' => "string",
        'update_unique_where' => "string"
    ])]
    public function persister(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->daoClass,
            'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            'create_unique_where' => $this->prepareCreateWhereCondition(),
            'update_unique_where' => $this->prepareUpdateWhereCondition(),
        ];
    }

    public function query(): array
    {
        return [];
    }
}
