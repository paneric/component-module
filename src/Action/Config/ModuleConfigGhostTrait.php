<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action\Config;

use Closure;
use JetBrains\PhpStorm\ArrayShape;
use PDO;

trait ModuleConfigGhostTrait
{
    use ModuleConfigTrait;

    #[ArrayShape([
        'table' => "string",
        'dto_class' => "string",
        'select_query' => "string",
        'fetch_mode' => "int"
    ])]
    public function repository(): array
    {
        return [
            'table' => $this->table,
            'dto_class' => $this->daoClass,
            'select_query' => $this->query,
            'fetch_mode' => PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
        ];
    }


    public function getOneById(): array
    {
        return [];
    }

    public function create(): array
    {
        return [];
    }

    public function createMultiple(): array
    {
        return [];
    }

    public function update(): array
    {
        return [];
    }

    public function updateMultiple(): array
    {
        return [];
    }

    public function delete(): array
    {
        return [];
    }

    public function deleteMultiple(): array
    {
        return [];
    }

    public function persister(): array
    {
        return [];
    }

    public function query(): array
    {
        return [];
    }
}
