<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Infrastructure\Event\DeleteFailureEvent;
use Paneric\ComponentModule\Infrastructure\Event\BeforeDeleteEvent;
use Paneric\ComponentModule\Infrastructure\Event\DeleteSuccessEvent;
use Paneric\ComponentModule\Interfaces\Action\DeleteApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\CSRTriad\Action;
use Psr\EventDispatcher\EventDispatcherInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

class DeleteApiAction extends Action implements DeleteApiActionInterface
{
    protected ModulePersisterInterface $adapter;
    protected array $config;
    protected EventDispatcherInterface $eventDispatcher;

    protected int $status;

    public function __construct(
        ModulePersisterInterface $adapter,
        ModuleConfigInterface $config,
        EventDispatcherInterface $eventDispatcher
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->delete();
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(Request $request, string $id): ?array
    {
        $deleteByCriteria = $this->config['delete_by_criteria'];

        $this->eventDispatcher->dispatch(new BeforeDeleteEvent($id));

        if ($this->adapter->delete($deleteByCriteria(['id' => $id])) === 0) {
            $this->eventDispatcher->dispatch(new DeleteFailureEvent($id));
            $this->status = 400;

            return  [
                'status' => $this->status,
                'error' => 'db_delete_error'
            ];
        }

        $this->eventDispatcher->dispatch(new DeleteSuccessEvent($id));

        $this->status = 200;

        return  [
            'status' => $this->status,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
