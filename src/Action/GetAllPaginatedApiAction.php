<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Action;

use Paneric\ComponentModule\Interfaces\Action\GetAllPaginatedApiActionInterface;
use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleRepositoryInterface;
use Paneric\CSRTriad\Action;
use Psr\Http\Message\ServerRequestInterface as Request;

class GetAllPaginatedApiAction extends Action implements GetAllPaginatedApiActionInterface
{
    protected ModuleRepositoryInterface $adapter;
    protected array $config;

    protected int $status;
    protected array $pagination;

    public function __construct(
        ModuleRepositoryInterface $adapter,
        ModuleConfigInterface $config
    ) {
        parent::__construct();

        $this->adapter = $adapter;
        $this->config = $config->getAllPaginated();
    }

    public function __invoke(Request $request, string $page): array
    {
        $this->status = 200;

        $pagination = $request->getAttribute('pagination');

        $queryParams = $request->getQueryParams();

        $findByCriteria = $this->config['find_by_criteria'];
        $orderBy = $this->config['order_by'];

        $collection = $this->adapter->findBy(
            $findByCriteria(),
            $orderBy($queryParams['field'] ?? 'id'),
            $pagination['limit'],
            $pagination['offset']
        );

        return [
            'status' => $this->status,
            'body' => $this->arrangeObjectsCollectionById($collection, true),
            'pagination' => $pagination,
        ];
    }

    public function getStatus(): int
    {
        return $this->status;
    }
}
