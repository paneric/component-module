<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Controller;

use Paneric\ComponentModule\Interfaces\Action\CreateApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\CreateMultipleApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\DeleteApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\DeleteMultipleApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\GetAllApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\GetAllByApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\GetAllByExtendedApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\GetOneByApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\QueryAllApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\GetAllPaginatedApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\GetOneByIdApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\UpdateApiActionInterface;
use Paneric\ComponentModule\Interfaces\Action\UpdateMultipleApiActionInterface;
use Paneric\ComponentModule\Interfaces\ActionListInterface;
use Paneric\Responder\Interfaces\ResponderInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ApiModuleController
{
    public function __construct(readonly protected ResponderInterface $responder)
    {
    }

    public function create(
        Request $request,
        Response $response,
        CreateApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function createMultiple(
        Request $request,
        Response $response,
        CreateMultipleApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function delete(
        Request $request,
        Response $response,
        DeleteApiActionInterface $action,
        string $id
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            $action($request, $id),
            $action->getStatus()
        );
    }

    public function deleteMultiple(
        Request $request,
        Response $response,
        DeleteMultipleApiActionInterface $action
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            $action($request),
            $action->getStatus()
        );
    }

    public function getAll(
        Request $request,
        Response $response,
        GetAllApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function getAllBy(
        Request $request,
        Response $response,
        GetAllByApiActionInterface $action,
        string $field,
        string $value,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request, $field, $value),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function getAllByExtended(
        Request $request,
        Response $response,
        GetAllByExtendedApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function getAllPaginated(
        Request $request,
        Response $response,
        GetAllPaginatedApiActionInterface $action,
        string $page,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request, $page),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function getOneById(
        Request $request,
        Response $response,
        GetOneByIdApiActionInterface $action,
        string $id,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request, $id),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function getOneBy(
        Request $request,
        Response $response,
        GetOneByApiActionInterface $action,
        string $field,
        string $value,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request, $field, $value),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus(),
        );
    }

    public function update(
        Request $request,
        Response $response,
        UpdateApiActionInterface $action,
        string $id,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request, $id),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function updateMultiple(
        Request $request,
        Response $response,
        UpdateMultipleApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }

    public function queryAll(
        Request $request,
        Response $response,
        QueryAllApiActionInterface $action,
        ActionListInterface $actionList = null
    ): Response {
        return ($this->responder)(
            $request,
            $response,
            array_merge(
                $action($request),
                ['list' => $actionList ? $actionList($request) : []]
            ),
            $action->getStatus()
        );
    }
}
