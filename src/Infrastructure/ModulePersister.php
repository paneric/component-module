<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Infrastructure;

use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModulePersisterInterface;
use Paneric\DBAL\Manager;
use Paneric\DBAL\Persister;

class ModulePersister extends Persister implements ModulePersisterInterface
{
    public function __construct(Manager $manager, ModuleConfigInterface $config)
    {
        parent::__construct($manager);

        $configValues = $config->persister();

        $this->table = $configValues['table'];
        $this->daoClass = $configValues['dto_class'];
        $this->fetchMode = $configValues['fetch_mode'];

        if (!empty($configValues['select_query'])) {
            $this->selectQuery = $configValues['select_query'];
        }

        $this->createUniqueWhere = $configValues['create_unique_where'];
        $this->updateUniqueWhere = $configValues['update_unique_where'];
    }
}
