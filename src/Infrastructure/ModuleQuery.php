<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Infrastructure;

use Paneric\ComponentModule\Interfaces\ModuleConfigInterface;
use Paneric\ComponentModule\Model\Interfaces\ModuleQueryInterface;
use Paneric\DBAL\Manager;
use Paneric\DBAL\Query;

class ModuleQuery extends Query implements ModuleQueryInterface
{
    public function __construct(Manager $manager, ModuleConfigInterface $config)
    {
        parent::__construct($manager);

        $configValues = $config->query();

        $this->adaoClass = $configValues['adto_class'];
        $this->baseQuery = $configValues['base_query'];
    }
}
