<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Infrastructure\Event;

use Paneric\Interfaces\Event\Event;
use Paneric\Interfaces\Event\EventInterface;

class CreateSuccessEvent extends Event implements EventInterface
{
}
