<?php

declare(strict_types=1);

namespace Paneric\ComponentModule\Adapter;

use App\config\GuardConfig;
use Paneric\ComponentModule\Interfaces\GuardInterface;
use RandomLib\Generator;

class Guard extends \Paneric\Guard\Guard implements GuardInterface
{
    public function __construct(Generator $randomGenerator, GuardConfig $config)
    {
        parent::__construct($randomGenerator, $config());
    }
}
