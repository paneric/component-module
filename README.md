# Component Module

### App\config\GuardConfig

*src/config/GuardConfig.php* (project folder)
```php
<?php

declare(strict_types=1);

namespace App\config;

class GuardConfig
{
    public function __invoke(): array
    {
        return [
            'key_ascii' => $_ENV['KEY_ASCII'],
            'algo_password' => PASSWORD_BCRYPT,
            'options_algo_password' => [
                'cost' => 10,
            ],
            'algo_hash' => 'sha512',
            'unique_id_prefix' => '',
            'unique_id_more_entropy' => true,
        ];
    }
}
```
